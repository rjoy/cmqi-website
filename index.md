The **Condensed Matter and Quantum Information** (CMQI) group at [The Institute of Mathematical Sciences](www.imsc.res.in) works in broad areas of quantum and classical condensed matter physics and quantum information.   

**Condensed matter physics** deals with the elucidation of properties of matter in all its phases, whether natural or artifically engineered. It covers a breathtaking range of exotic phenomena over the full range of length scales. To give an example, superconductivity, a macroscopic quantum coherence phenomenon, spans length scales ranging from millimeter sized Josephson junction arrays through macro-sized materials like cuprates, to superfluidity in neutron stars.

Like all sciences, condensed matter physics seens to discern the basic organizing principles underlying connected set of phenomena. These facilitate deeper and fruitful studies of **emergent phenomena**, those that emerge solely as a consequence of collective behaviour involving a macroscopically large number of microscopic building blocks. In striking illustrations of the *The whole is more than the sum of its parts* such phenomena are often wholly unanticipated. An ability to understand the process underpins the ability to manipulate forms of matter, often for technological uses. 

[Ganesh's edit of above paragraph] Condensed matter physics seeks to develop basic organizing principles that underlie a wide variety of phenomena. A ce[nral recurring theme is 'emergence': new properties that
emerge as a consequence of collective behaviour involving a large number of microscopic building blocks. The whole is more than the sum of its parts!
By understanding such behaviour, we are able to devise new forms of matter, often with great technological potential.


At [CMQI](), we seek to devise and exploit modern methods of theoretical physics and mathematics, from formal and intricate classical and quantum field theoretic tools to equally involved state-of-the-art numerical techniques, to investigate a wide spectrum of problems. These range from unconventional *liquid* quantum and classical states of matter, superconductivity and novel metallicity, the dynamics of topological defects in ordered media, to topological characterization of quantum phase transitions and fundamental issues in quantum information. A fine balance between *pure* and *real-world* problems characterizes our research, as does an interdisciplinary approach to complex systems where different organizing principles can emerge at different length scales. 

---

### Research Themes

CMQI presents a confluence of three subgroups. Each of these seeks to bring their own understanding and vision. These are

**Quantum condensed matter physics**

QCMP is in the throes of "crisis", shorthand for the ong going revolution of ideas that have emernged in response to the 
challenges posed by the discovery of new materials whose behaviour is ill-understood on a fundamental level. After a quarter
century of intense debate, even the most fundamental ideas remain open avenues of inquiry. Simultaneously, however, the
paradigm shift this has entailed has resulting in a much deeper appreciation and understanding of nrew complex materials,
as well as the limits of applicability of traditional approaches.

ON the theoretical side, this crisis has spawned radically new ideas; among them, the most radical and far-reaching ones are 
quantum critical liquid states of matter, topological order, emergent fractionalized quasiparticle, and the like. This state
of flux means that the field is wide open for even more novel discoveries : what has hiterto been revealed may well be overwhelmed
by more surprises as we dig deeper. 

At QCMP we bring a judicious mix of analytic many-particle field-theoretic and advanced numerical skills to bear upon these
issues. Simultaneously, an equally strong component of our research also focusses on real-world complex materials showing
novel unanticipated behaviour. For more details of our activities, please follow [this link](). [**Note** : what is this to link to ?]

---

## Faculty
We are a group of eight faculty members with constructive overlaps. We have strong collaborations within our group that includes joint supervision of Ph.D students.

### Condensed matter physics

* [Ronojoy Adhikari](www.imsc.res.in/~rjoy) - soft solids, complex fluids.
* [Pinaki Chaudhuri](www.imsc.res.in/~pinakic) - glasses and disordered materials
* [Syed R. Hassan](www.imsc.res.in/~srhassan) - quantum condensed matter
* [Mukul S. Laad](www.imsc.res.in/~mslaad) - quantum condensed matter
* [Ganesh Ramachandran](www.imsc.res.in/~ganesh) - quantum condensed matter
* [R. Shankar](www.imsc.res.in/~shankar) - quantum condensed matter

### Quantum information theory

* [Chandrasekhar Madiah](www.imsc.res.in/~chandru) - quantum information
* [Sibasish Ghose](www.imsc.res.in/~sibasish) - quantum information

### Distinguished Associate Faculty

* [R. Simon](www.imsc.res.in/~simon) - classical optics, quantum information
* [G. Baskaran](www.imsc.res.in/~gbaskaran) - strongly correlated condensed matter

---

## Students

* [Abhrajit Laskar]()
* [Arya]()
* [Srilucksmy Viswanathan]()
* [Rajesh Singh]()

---

## Working with us

We provide a stimulating environment for Ph.D students who want to work in theoretical condensed matter physics and quantum information theory. 

### Projects

* **Dynamics of non-Abelian topological defects in spinor condensates** : [Ronojoy Adhikari](www.imsc.res.in/~rjoy) and [Syed R. Hassan](www.imsc.res.in/~srhassan). 
* 

---

## Teaching 

We take our role as teachers and mentors very seriously. We discuss coursework amongst ourselves and design courses that are coherent, up-to-date, and structured to introduce a beginning student to frontiers of research in the fastest possible manner. 

r* Statistical Physics 1 - Semester 1 (2015) - [Ronojoy Adhikari]()
* Condensted Matter Physics - Semester 1 (2015) [Mukul S. Laad]()
* Advanced Condensed Matter Physics - Semester [R. Shankar]

## Special lecture series

* **Long wavelength approximations in condensed matter physics** : will be taught jointly by Ronojoy Adhikari, Syed R. Hassan and Mukul S. Laad. This course will teach the basics of long wavelength approximation, including broken symmetry, hydrodynamic fluctuations and so on. 



